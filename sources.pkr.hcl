source "amazon-ebs" "windows-2016-base" {
  region = var.region
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
  token = var.aws_session_token

  source_ami_filter {
    filters = {
       virtualization-type = "hvm"
       name = "Windows_Server-2016-English-Full-Base*"
       root-device-type = "ebs"
			 platform = "windows"
    }
    owners = ["amazon"]
    most_recent = true
  }

  instance_type = "t2.medium"
  #ssh_username = "administrator"
  ami_name = "base-windows-{{timestamp}}"

	communicator = "winrm"
	winrm_use_ssl = true
	winrm_insecure = true
	winrm_timeout = "15m"
	winrm_username = "packer"
	winrm_password = "HashiCorp123!"

	user_data_file = "userdata.txt"

  tags = {
    owner = var.owner
    application = "base-build"
    Base_AMI_Name = "{{ .SourceAMIName }}"
  }
}