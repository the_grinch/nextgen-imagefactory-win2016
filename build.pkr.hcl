build {
  sources = [
    "source.amazon-ebs.windows-2016-base"
  ]

	provisioner "ansible" {
    playbook_file = "./playbook.yaml"
    user = "administrator"
		use_proxy = false
    extra_arguments = ["-e -ansible_python_interpreter=/usr/bin/python3"]
		ansible_env_vars = ["no_proxy=\"*\""]
  }
}